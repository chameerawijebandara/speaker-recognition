function varargout = Sharks(varargin)
global mfcc_file
global sbc_file
global wav_file
global signal
global cha_colour
  global max_1
  global max_2
  global max_3
  global i_max_1
  global i_max_2
  global i_max_3

% SHARKS M-file for Sharks.fig

% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Sharks

% Last Modified by GUIDE v2.5 27-May-2014 12:12:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Sharks_OpeningFcn, ...
                   'gui_OutputFcn',  @Sharks_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Sharks is made visible.
function Sharks_OpeningFcn(hObject, eventdata, handles, varargin)
% Choose default command line output for Sharks
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes Sharks wait for user response (see UIRESUME)
% uiwait(handles.sharks);
% splash('splash.jpg');









% --- Outputs from this function are returned to the command line.
function varargout = Sharks_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ah = axes('unit', 'normalized', 'position', [0 0 1 1]); 
% import the background image and show it on the axes
bg = imread('background.jpg'); imagesc(bg);
% prevent plotting over the background and turn the axis off
set(ah,'handlevisibility','off','visible','off')
% making sure the background is behind all the other uicontrols
uistack(ah, 'bottom');
% Get default command line output from handles structure
varargout{1} = handles.output;
if ~exist( 'config.mat')
         warndlg('Default Configuration file not found select features file to use or create freash database','Error');
        no_of_fe=0;
end;
set(gcf,'CloseRequestFcn',@sharks_closefcn)



function sharks_closefcn(src,evnt)
global mfcc_file
global sbc_file

 selection = questdlg('Want to QUIT SHARKS',...
      'Close Request Function',...
      'Yes','No','Yes'); 
   switch selection, 
      case 'Yes',
         save('config.mat','mfcc_file','sbc_file');
         delete(gcf)
      case 'No'
      return 
   end



function m_Exit_Callback(hObject, eventdata, handles)
global mfcc_file
global sbc_file

 selection = questdlg('Want to QUIT SHARKS',...
      'Close Request Function',...
      'Yes','No','Yes'); 
   switch selection, 
      case 'Yes',
         save('config.mat','mfcc_file','sbc_file');
         delete(gcf)
      case 'No'
      return 
   end


% --- Executes on button press in bt_extract.
function bt_extract_Callback(hObject, eventdata, handles)
% hObject    handle to bt_extract (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global mfcc_file
global sbc_file
global wav_file
global signal

per_name=get(handles.tx_name,'String');
if (ischar(per_name)&(length(per_name)>1))
    load(mfcc_file);
    if (no_of_fe==0) 
        name={''} ;
    end
        
    
    if any(strcmp(per_name,name)>0)  % find if the new name already exists in data base
    errordlg('name Already exists');
    else
    
    
        selection = questdlg(strcat('Save the Voice with name as :',per_name),...
            'Close Request Function',...
            'Yes','No','Yes'); 
        switch selection, 
            case 'Yes',
         
    
                if (length(signal)<10)|(length(mfcc_file)<2)
                 errordlg('Signal or Feature File to inject not sellected');
                else
                    set(handles.tx_msg,'String','Extracting MFCC Features');   
                    MFCC_feat_inject(signal,mfcc_file,per_name);
                    set(handles.tx_msg,'String','');    

                end

             case 'No'
                return 
          end% end of switch
        
        end % end for if to check duplicate name 
else
errordlg('Enter Proper name');
     
end





function plot_sig()
global signal
            samp_len = length(signal)/8000;
            delta_t = 1/8000;
            t = 0:delta_t:(samp_len-delta_t);

            % display the signal
            plot(t,signal), xlabel('Time [sec]'), ylabel('Amplitude')
            axis([0 t(length(signal)-1) -1 1 ]);

            


% Hint: get(hObject,'Value') returns toggle state of ch_MFCC


% --- Executes on button press in bt_play.
function bt_play_Callback(hObject, eventdata, handles)

global signal
% hObject    handle to bt_play (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if length(signal) ~= 0
      wavplay(signal,8000)
end


% --- Executes during object creation, after setting all properties.
function tx_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tx_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function tx_name_Callback(hObject, eventdata, handles)
% hObject    handle to tx_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tx_name as text
%        str2double(get(hObject,'String')) returns contents of tx_name as a double



% --- Executes on button press in btn_recode.
function btn_recode_Callback(hObject, eventdata, handles)
% hObject    handle to btn_recode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global signal
global max_1
global max_2
global max_3
global i_max_1
global i_max_2
global i_max_3
recording_duration = 3;
R_samp_len = recording_duration;

set(handles.tx_msg,'Visible','on');
set(handles.tx_msg,'ForegroundColor',[1 0 0],'string','Recording..');

data=wavrecord(8000*R_samp_len,8000);% recording the sound
set(handles.tx_msg,'ForegroundColor',[0 0 0],'String','Recording finished');   
signal = 0.99*data/max(abs(data));
plot_sig;

% --- Executes on button press in btn_database.
function btn_database_Callback(hObject, eventdata, handles)
% hObject    handle to btn_database (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
db_manage;


% --- Executes on button press in btn_logs.
function btn_logs_Callback(hObject, eventdata, handles)
% hObject    handle to btn_logs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
popupmessage('logs.txt','Attendants Logs');

% --- Executes on button press in btn_errorLogs.
function btn_errorLogs_Callback(hObject, eventdata, handles)
% hObject    handle to btn_errorLogs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
popupmessage('errorLogs.txt','Error Logs');
