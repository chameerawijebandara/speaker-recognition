function varargout = loro(varargin)
% LORO M-file for loro.fig
%      LORO, by itself, creates a new LORO or raises the existing
%      singleton*.
%
%      H = LORO returns the handle to a new LORO or the handle to
%      the existing singleton*.
%
%      LORO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LORO.M with the given input arguments.
%
%      LORO('Property','Value',...) creates a new LORO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before loro_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to loro_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help loro

% Last Modified by GUIDE v2.5 22-Apr-2014 23:53:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @loro_OpeningFcn, ...
                   'gui_OutputFcn',  @loro_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before loro is made visible.
function loro_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to loro (see VARARGIN)

% Choose default command line output for loro
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes loro wait for user response (see UIRESUME)
% uiwait(handles.figure1);
% create an axes that spans the whole gui
ah = axes('unit', 'normalized', 'position', [0 0 1 1]); 
% import the background image and show it on the axes
bg = imread('background.jpg'); imagesc(bg);
% prevent plotting over the background and turn the axis off
set(ah,'handlevisibility','off','visible','off')
% making sure the background is behind all the other uicontrols
uistack(ah, 'bottom');
imshow('logo.jpg','Parent',handles.logo);



timeCounter = timer('ExecutionMode','fixedRate',...
                    'Period', 1,...
                    'TimerFcn', {@GUIUpdate,handles});
start(timeCounter);
                
function GUIUpdate(obj,event,handles)
    set(handles.txt_date,'string',date);
    set(handles.txt_time,'string',datestr(now, 'HH:MM:SS'));


% --- Outputs from this function are returned to the command line.
function varargout = loro_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btn_user.
function btn_user_Callback(hObject, eventdata, handles)
% hObject    handle to btn_user (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
gui();

% --- Executes on button press in btn_admin.
function btn_admin_Callback(hObject, eventdata, handles)
% hObject    handle to btn_admin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
password = logindlg('Title','Please Enter the Admin Password','Password','only');
while(~strcmp(password,  '1234'))
    if isempty(password)
        return;
    end    
    password = logindlg('Title','Please Enter the Correct Password','Password','only');
    
end    
Sharks();

% --- Executes on button press in btn_help.
function btn_help_Callback(hObject, eventdata, handles)
% hObject    handle to btn_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Help();

% --- Executes on button press in btn_about.
function btn_about_Callback(hObject, eventdata, handles)
% hObject    handle to btn_about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
About();
