function varargout = gui(varargin)
global signal RECT s_plot  
global max_1
global max_2
global max_3
global i_max_1
global i_max_2
global i_max_3

% GUI M-file for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 20-Apr-2014 22:07:34


% signal speach signal 
% Rect is the coordenates used for getting the waveform

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT



% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
ah = axes('unit', 'normalized', 'position', [0 0 1 1]); 
% import the background image and show it on the axes
bg = imread('background.jpg'); imagesc(bg);
% prevent plotting over the background and turn the axis off
set(ah,'handlevisibility','off','visible','off')
% making sure the background is behind all the other uicontrols
uistack(ah, 'bottom');

t = timer('ExecutionMode','fixedRate',...
                    'Period', 1,...
                    'TimerFcn', {@GUIUpdate,handles});
start(t);
                
function GUIUpdate(obj,event,handles)
set(handles.txt_date,'string',date);
set(handles.txt_time,'string',datestr(now, 'HH:MM:SS'));



% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in bt_record.
function bt_record_Callback(hObject, eventdata, handles)
global signal
global max_1
global max_2
global max_3
global i_max_1
global i_max_2
global i_max_3
recording_duration = 3;
R_samp_len = recording_duration;

set(handles.tx_msg,'ForegroundColor',[1 0 0],'string','Recording..');


data=wavrecord(8000*R_samp_len,8000);% recording the sound
set(handles.tx_msg,'ForegroundColor',[0 0 0],'String','Recording finished');   
signal = 0.99*data/max(abs(data));
plot_sig;

set(handles.tx_result,'visible','off');
set(handles.tx_person_is,'visible','off');
set(handles.tx_Match_Quality,'visible','off');
set(handles.tx_Quality,'visible','off');
set(handles.bt_Details,'visible','off');
% now code to plot

load('config.mat');

match_v= MFCC_feature_compare(signal,mfcc_file);
load(mfcc_file);
 
amax=-1000;

[max_1 i_max_1]=max(match_v);    % Find the index of the person with closest match
match_v(i_max_1)=[];             % Remove it to next value
[max_2 i_max_2]=max(match_v);    % Find the index of the person with next closest match
match_v(i_max_2)=[];             % Remove it to next value
[max_3 i_max_3]=max(match_v);




if ~exist( 'logs.txt')
    fileIDLog = fopen('logs.txt','w');
    fprintf(fileIDLog,'%s \t\t\t %s \t\t %s\n','Date','Time','User Name');
    fclose(fileIDLog);
end
if ~exist( 'errorLogs.txt')
    fileIDErrorLog = fopen('errorLogs.txt','w');
    fprintf(fileIDErrorLog,'%s \t\t\t %s \t\t %s\n','Date','Time','Error');
    fclose(fileIDErrorLog);
end

fileIDLog = fopen('logs.txt','a');
fileIDErrorLog = fopen('errorLogs.txt','a');

if max_1>-13.6
 set(handles.tx_Quality,'string','PERFECT');
 set(handles.tx_result,'string',name(i_max_1,:),'visible','on');
 fprintf(fileIDLog,'%s \t %s \t %s\n',date,datestr(now, 'HH:MM:SS'),name(i_max_1,:));
else
 set(handles.tx_Quality,'string','POOR');
 set(handles.tx_result,'string','UNKNOWN(REF DETAILS)','visible','on');
 fprintf(fileIDErrorLog,'%s \t %s \t %s\n',date,datestr(now, 'HH:MM:SS'),'UNKNOWN(REF DETAILS)');
end

fclose(fileIDLog);
fclose(fileIDErrorLog);

set(handles.tx_person_is,'visible','on');
set(handles.tx_Match_Quality,'visible','on');
%set(handles.bt_Details,'visible','on');
set(handles.tx_Quality,'Visible','on');

if(no_of_fe<3)
 set(handles.bt_Details,'visible','off');
end



% hObject    handle to bt_record (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% --- Executes on button press in bt_play_select.
function bt_play_select_Callback(hObject, eventdata, handles)

global signal
% hObject    handle to bt_del_select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
      

   % Allow the user to draw a rectangle on the area
   % they would like to zoom in on
   RECT = getrect;
   
   xmin = RECT(1);
   xmax = RECT(1) + RECT(3);
   ymin = RECT(2);
   ymax = RECT(2) + RECT(4);
   
   % Set maximum zoom limits to the data edges
   xaxis_limits = get(handles.s_plot,'XLim');
   yaxis_limits = get(handles.s_plot,'YLim');
    yaxis_limits(2);
    xaxis_limits(2);
   
   if xmin < xaxis_limits(1)
      xmin = xaxis_limits(1);
   end
   
   if xmax > xaxis_limits(2)
      xmax = xaxis_limits(2);
      
   end

   if ymin < yaxis_limits(1)
      ymin = yaxis_limits(1);
   end
   
   if ymax > yaxis_limits(2)
      ymax = yaxis_limits(2);
       yaxis_limits(2);
   end
   
   % if the choosen zoom range is acceptable...
   if ~((ymin > ymax) || (xmin > xmax))  
     
      % zoom in on the frequency data by adjusting the xaxis
      % limits to be the same as those of the time data
      % define the zoomed in data (for playback purposes)
      imin = round(xmin*8000)+1;
      imax = round(xmax*8000)+1;
  end
    if ~isempty(signal(imin:imax) )
      wavplay(signal(imin:imax),8000);
    end



% --- Executes during object creation, after setting all properties.
function tb_save_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tb_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes during object creation, after setting all properties.
function tb_load_file_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tb_load_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end   
      

% --- playing entire file
function bt_play_Callback(hObject, eventdata, handles)
% hObject    handle to bt_close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global signal
    % hObject    handle to bt_play (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    if ~isempty(signal)
          wavplay(signal,8000)
    end  
    
function plot_sig()

    global signal
    samp_len = length(signal)/8000;
    delta_t = 1/8000;
    t = 0:delta_t:(samp_len-delta_t);

    % display the signal
    plot(t,signal), xlabel('Time [sec]'), ylabel('Amplitude')
    axis([0 t(length(signal)-1) -1 1 ]);



% --- Executes on button press in bt_close.
function bt_close_Callback(hObject, eventdata, handles)
% hObject    handle to bt_close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    close;


function bt_Details_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to bt_Details (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global max_1
global max_2
global max_3
global i_max_1
global i_max_2
global i_max_3

load(mfcc_file);
result( name(i_max_1,:),max_1,name(i_max_2,:),max_2,name(i_max_3,:),max_3);


% --- Executes on button press in bt_Details.
function bt_Details_Callback(hObject, eventdata, handles)
% hObject    handle to bt_Details (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global max_1
global max_2
global max_3
global i_max_1
global i_max_2
global i_max_3

load('config.mat');

load(mfcc_file);
result( name(i_max_1,:),max_1,name(i_max_2,:),max_2,name(i_max_3,:),max_3);
