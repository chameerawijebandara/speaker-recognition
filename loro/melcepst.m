function c=melcepst(s,fs)
%MELCEPST Calculate the mel cepstrum of a signal C=(S,FS,W,NC,P,N,INC,FL,FH)

if nargin<2 fs=8000; end
w='M';
 nc=12;
 p=floor(3*log(fs)); %number of coifetions
 n=pow2(floor(log2(0.03*fs))); %25 filters in the Mel bank

   fh=0.5;   
   fl=0;
   inc=floor(n/2); %cepstral coefficients
    
  
if length(w)==0
   w='M';
end

 %figure(2);
 %  subplot(3,3,1);
 %  plot(s);
 %  xlabel('Time [sec]');
 %  ylabel('Amplitude');
  % title('Original waveform')


%ENFRAME split signal up into (overlapping) frames: one per row. F=(X,WIN,INC)
   z=enframe(s,hamming(n),inc);
   
   
  % subplot(3,3,2);
 %  plot(z(1,:));
  % xlabel('Time [sec]');
  % ylabel('Amplitude');
   %title('Frame blocking')
   
  % subplot(3,3,3);
  % plot(hamming(n));
  % xlabel('Time [sec]');
  % ylabel('Amplitude');
  % title('Hamming window')
   

%FFT
f=rfft(z.');
%subplot(3,3,4)

   %plot(f);
   %xlabel('Time [sec]');
   %ylabel('Amplitude');
   %title('After FFT');

%MELBANKM determine matrix for a mel-spaced filterbank [X,MN,MX]=(P,N,FS,FL,FH,W)
[m,a,b]=melbankm(p,n,fs,fl,fh,w);
%subplot(3,3,5)
   %plot(m);
   %xlabel('Time [sec]');
   %ylabel('Amplitude');
   %title('Mel-spaced filterbank');


%Apply the mel filterbank to the power spectra, sum the energy in each filter.
pw=f(a:b,:).*conj(f(a:b,:));
pth=max(pw(:))*1E-6;
ath=sqrt(pth);

%subplot(3,3,6)
 %  plot(pw(1,:));
  % xlabel('Time [sec]');
  % ylabel('Amplitude');
  % title('After apply the mel filterbank to the power spectra');

%Take the logarithm of all filterbank energies.
   y=log(max(m*abs(f(a:b,:)),ath));
   
   %subplot(3,3,7)
   %plot(y(1,:));
   %xlabel('Time [sec]');
   %ylabel('Amplitude');
   %title('After take the logarithm of all filterbank energies');

%RDCT     Discrete cosine transform of real data Y=(X,N,A,B)
c=rdct(y).';
nf=size(c,1);
nc=nc+1;

y=log(max(m*abs(f(a:b,:)),ath));
   
  % subplot(3,3,8)
  % plot(c(1,:));
  % xlabel('Time [sec]');
  % ylabel('Amplitude');
  % title('After discrete cosine transform ');
if p>nc
   c(:,nc+1:end)=[];
elseif p<nc
   c=[c zeros(nf,nc-p)];
end

%subplot(3,3,9)
 %  plot(c(1,:));
 %  xlabel('Time [sec]');
 %  ylabel('Amplitude');
 %  title('Mel cepstrum of a signal ');


